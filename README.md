# Decoupled Preview

Preview Drupal content on your decoupled front end site.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/decoupled_preview).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/decoupled_preview).

### Features

- Preview published content.
- Preview while editing a node.
- Preview revisions.
- Configure multiple preview sites.
- Specify the content types that each preview site applies to.
- Specify an OAuth consumer related to a preview site.

## Requirements

This module requires the following modules:

- [decoupled_router](https://www.drupal.org/project/issues/decoupled_router)
- [jsonapi_resources](https://www.drupal.org/project/issues/jsonapi_resources)
- [simple_oauth](https://www.drupal.org/project/issues/simple_oauth)

## Installation

Require using composer:

`composer require drupal/decoupled_preview`

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This module requires a [patch to the Decoupled Router module](https://www.drupal.org/files/issues/2021-05-05/3111456-34.patch). If you install this module with composer, [allow patches to be applied from dependencies](https://github.com/cweagans/composer-patches#allowing-patches-to-be-applied-from-dependencies) and require `cweagans/composer-patches` in the root of the project `composer.json` file. The patch will be applied automatically.

Navigate to Structure -> Preview Sites (`/admin/structure/dp-preview-site`), add
one or more preview sites, and configure the following:

- Label: The name of the site.
- Preview URL: The URL of the decoupled site you are providing preview data to.
- Preview Secret: A token that will be passed to your decoupled site used to
  limit access to the preview.
- Preview Type: The type of preview site - currently only Next.js is supported.
- Content Types: The content types that this preview site applies to.

## Preview Types

While we hope to expand in the future, the initial release of this module only
supports Next.js. It was developed in support of [Pantheon's Next Drupal Starter](https://github.com/pantheon-systems/next-drupal-starter), but can be applied to other
Next.js sites using a similar approach.
